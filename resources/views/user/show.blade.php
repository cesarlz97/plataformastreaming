@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('css/video-js.css')}}">
@endsection

@section('content')

<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>{{ $user->name }}</h1>
      <h2>{{ $user->email }}</h2>   
      @if (auth()->user()->id == $user->id)          
    <p>Tu URL de emisión es: rtmp://3.15.67.140/LiveApp/{{$user->id}}</p>    
      @endif  
      <video id='my-video' class='video-js vjs-default-skin vjs-big-play-centered' width="720" height="auto" controls preload='auto'>
      </video>
  </div>
</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/video-js.js') }}" ></script>
<script>  
  var player = videojs("my-video");
    player.src({
      src:
        "http://3.15.67.140:5080/LiveApp/streams/" + {{ $user->id }} + ".m3u8",
      type: "application/x-mpegURL",
  });
</script>
@endsection
